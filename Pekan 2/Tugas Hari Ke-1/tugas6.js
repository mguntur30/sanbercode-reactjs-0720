//SOAL NOMOR 1
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
var objPeserta = {nama : "John", jenisKelamin : "laki-laki",hobby : "baca buku",tahunLahir : 1992}
console.log(objPeserta);

//SOAL NOMOR 2
var buah = [
    {nama : "strawberry",warna : "merah","ada bijinya" : "tidak",harga : 9000},
    {nama : "jeruk",warna : "oranye","ada bijinya" : "ada",harga : 8000},
    {nama : "semangka",warna : "hijau & merah","ada bijinya" : "ada",harga : 10000},
    {nama : "pisang", warna : "kuning","ada bijinya" : "tidak",harga : 5000 }
]
console.log(buah[0]);

//SOAL NOMOR 3 
var dataFilm = []
var film = {
    nama : "film Guntur",
    durasi : 120,
    genre : "Tech",
    tahun : 2000
}
function pushFilm(film){
    dataFilm.push(film)
    return dataFilm
    
};
console.log(pushFilm(film))

//SOAL NOMOR 4
class Animal {
    
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

//SOAL NOMOR 5
function Clock({ template }) {
  
    var timer;
  
    function render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    this.stop = function() {
      clearInterval(timer);
    };
  
    this.start = function() {
      render();
      timer = setInterval(render, 1000);
    };
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 
  