//SOAL NOMOR 1
var kataPertama = "saya";
var kataKedua = "Senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";
console.log(kataPertama+" "+kataKedua+" "+kataKetiga+" "+kataKeempat.toUpperCase());
console.log(); */


//SOAL NOMOR 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var a = parseInt(kataPertama);
var b = parseInt(kataKedua);
var c = parseInt(kataKetiga);
var d = parseInt(kataKeempat);

console.log(a+b+c+d);



//SOAL NOMOR 3
var kalimat = 'wah javascript itu keren sekali'; 
var kataPertama = kalimat.substring(0,3); 
var kataKedua=kalimat.substring(4,14); // do your own! 
var kataKetiga=kalimat.substring(15,18); // do your own! 
var kataKeempat=kalimat.substring(19,25); // do your own! 
var kataKelima=kalimat.substring(25,31); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);



//SOAL NOMOR 4
var nilai = 75;

if (nilai >= 80) {
    console.log("Nilai A");

} else if (nilai >= 70 && nilai < 80) {
    console.log("Nilai B");

} else if (nilai >= 60 && nilai < 70) {
    console.log("Nilai C");
}else if(nilai >= 50 && nilai < 60){
    console.log("Nilai D");
    
}else if(nilai < 50){
    console.log("Nilai E");
    
}else{
    console.log("Nilai tidak tersedia");
    
}

//SOAL NOMOR 5
var tanggal = 30;
var bulan = 8;
var tahun = 2000;

switch(bulan){
    case 1 : {console.log(tanggal+" Januari "+tahun);break; }
    case 2 : {console.log(tanggal+" Februari "+tahun);break;}
    case 3 : {console.log(tanggal+" Maret "+tahun);break;}
    case 4 : {console.log(tanggal+" April "+tahun);break;}
    case 5 : {console.log(tanggal+" Mei "+tahun); break;}
    case 6 : { console.log(tanggal+" Juni "+tahun); break;}
    case 7 : { console.log(tanggal+" Juli "+tahun);break;}
    case 8 : {console.log(tanggal+" Agustus "+tahun);break;}
    default : { console.log("bulan tidak tersedia");  }
}




