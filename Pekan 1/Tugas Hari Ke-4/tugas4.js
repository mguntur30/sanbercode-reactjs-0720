//SOAL NOMOR 1
var iterasi = 1
console.log("LOOPING PERTAMA");
while (iterasi < 20) {
    iterasi++
    if (iterasi % 2 == 0) {

        console.log(iterasi + " - I Love Coding");
    }
}
var iterasi2 = 21
console.log("LOOPING KEDUA");

while (iterasi2 <= 21 && iterasi2 > 1) {
    iterasi2--
    if (iterasi2 % 2 == 0) {
        console.log(iterasi2 + "  -I will become a frontend developer");

    }
}
//SOAL NOMOR 2
console.log();
console.log("======================================================");
for (var angka = 1; angka <= 20; angka++) {
    if (angka % 2 == 1 && angka % 3 == 0) {
        console.log(angka + " -I Love Coding");

    }
    if (angka % 2 == 1) {
        console.log(angka + " -Santai");
    }
    if (angka % 2 == 0) {
        console.log(angka + " -Berkualitas");
    }
}
console.log();
console.log("=====================================");
var x = ''
for (var y = 1; y <= 7; y++) {
    for (var z = 1; z <= y; z++) {
        x += '#'
    }
    x += '\n'
}
console.log(x);
console.log();
console.log("=====================================");
//SOAL NOMOR 4
var kalimat = "saya sangat senang belajar javascript"
var arrKalimat = kalimat.split(" ")
console.log(arrKalimat);
console.log("=====================================");
console.log();
//SOAL NOMOR 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort()
for (var x = 0; x < 5; x++) {
    console.log(daftarBuah[x]);

}




